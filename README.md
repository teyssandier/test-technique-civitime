# Tests techniques Civitime

## Présentation

    Ce test technique comporte deux parties:
    - un test qui porera sur de l'algorithmie
    - un test qui portera sur de l'intégration d'UIs
    
    Vous trouverez dans chaque dossier un README qui vous présentera les consignes 
    et des ressources pour vous permettre de compléter ces tests.
    
    Une fois que votre test est terminé, je vous propose de créer une branche comportant votre
    nom/prénom, et de pousser votre code sur cette branche

- <strong>git checkout -b nom-prénom</strong> => créer sa branche
- <strong>git add . </strong> => ajouter les fichiers modifiés. Bien faire cette commande à la racine du projet
- <strong>git push -u origin {votre branche => npm-prénom} </strong> => envoyer vos modifications sur le repo distant


### Bon courage à vous !