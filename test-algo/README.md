##Test-algo

    Bonjour,

    Bienvenue dans ce test technique.
    Celui-ci porte sur la capacité à retracer l'action d'un joueur sur un clicker 
    
    style cookie clicker:

https://orteil.dashnet.org/cookieclicker

    Vous trouverez dans ce dossier test-algo deux fichiers:

        - Le fichier events.json qui comporte un tableau d'évènements liés à un joueur. Il existe 2 types d'évènements,
          les évènements de click (PlayerClicked) et les évènements d'achat (PlayerBought)
        - Le fichier index.js, dans lequel vous allez pouvoir écrire le code pour répondre aux consignes. La première fonction
          est déjà écrite et pourra vous servir d'exemple.



###Consignes:

    1 - Comptez tous les événements d'incrément de clic (1 clic = 1 point) (exemple)

    2 - Reprenez votre fonction précédente pour compter les incréments de clic. Pour chaque évènement d'achat,
        décrémentez les points du joueur et comptez les différents objets achetés (Hand ou GrandMa)

    3 - Nouvelle règle: pour chaque évènement de clic, si le joueur possède des mains (Hand), la valeur de son clic est égale à:
        
        1 + (nombre de main(s) possédée(s))
        
        Retournez le nombre de points final du joueur, ainsi que ses différentes possessions (Hand et GrandMa)


    Pour lancer le fichier, vous pouvez utiliser node (`node index.js` dans le terminal)

    Pour télécharger node si ce n'est pas déja fait: 
    
https://nodejs.org/en/download/